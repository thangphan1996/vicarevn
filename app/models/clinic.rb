class Clinic < ApplicationRecord
  has_many :doctor_clinics, dependent: :destroy
  has_many :doctors, through: :doctor_clinics

  has_many :specialist_clinics, dependent: :destroy
  has_many :specialists, through: :specialist_clinics
  has_many :comments, dependent: :destroy

  mount_uploader :image, ImageUploader
end
