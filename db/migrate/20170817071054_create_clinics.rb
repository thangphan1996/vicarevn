class CreateClinics < ActiveRecord::Migration[5.1]
  def change
    create_table :clinics do |t|
      t.string :name
      t.string :address
      t.string :service
      t.text :introduce
      t.string :time_open
      t.string :time_close
      t.references :doctor, foreign_key: true

      t.timestamps
    end
  end
end
