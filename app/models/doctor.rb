class Doctor < ApplicationRecord
  has_many :specialists
  has_many :comments, dependent: :destroy

  has_many :doctor_clinics, dependent: :destroy
  has_many :clinics, through: :doctor_clinics

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    name_changed?
  end

  mount_uploader :avatar, AvatarUploader
end
