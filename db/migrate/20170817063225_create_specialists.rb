class CreateSpecialists < ActiveRecord::Migration[5.1]
  def change
    create_table :specialists do |t|
      t.string :name
      t.integer :count
      t.text :body
      t.references :doctor, foreign_key: true

      t.timestamps
    end
  end
end
