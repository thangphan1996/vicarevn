require "administrate/field/base"

class AvatarField < Administrate::Field::Base
  def url
    data.url
  end

  def thumb
    data.url(:thumb)
  end

  def sm_thumb
    data.url(:sm_thumb)
  end

  def to_s
    data
  end
end
