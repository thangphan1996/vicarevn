class CreateDoctorClinics < ActiveRecord::Migration[5.1]
  def change
    create_table :doctor_clinics do |t|
      t.references :doctor, foreign_key: true
      t.references :clinic, foreign_key: true

      t.timestamps
    end
  end
end
