# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170825014737) do

  create_table "clinics", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "service"
    t.text "introduce"
    t.string "time_open"
    t.string "time_close"
    t.integer "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.index ["doctor_id"], name: "index_clinics_on_doctor_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "content"
    t.integer "doctor_id"
    t.integer "clinic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clinic_id"], name: "index_comments_on_clinic_id"
    t.index ["doctor_id"], name: "index_comments_on_doctor_id"
  end

  create_table "doctor_clinics", force: :cascade do |t|
    t.integer "doctor_id"
    t.integer "clinic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clinic_id"], name: "index_doctor_clinics_on_clinic_id"
    t.index ["doctor_id"], name: "index_doctor_clinics_on_doctor_id"
  end

  create_table "doctors", force: :cascade do |t|
    t.string "name"
    t.string "avatar"
    t.string "job"
    t.string "experience"
    t.string "specialized"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "specialist_clinics", force: :cascade do |t|
    t.integer "specialist_id"
    t.integer "clinic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clinic_id"], name: "index_specialist_clinics_on_clinic_id"
    t.index ["specialist_id"], name: "index_specialist_clinics_on_specialist_id"
  end

  create_table "specialists", force: :cascade do |t|
    t.string "name"
    t.integer "count"
    t.text "body"
    t.integer "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_specialists_on_doctor_id"
  end

end
