Rails.application.routes.draw do
  mount Ckeditor::Engine => "/ckeditor"
  namespace :admin do
    resources :comments
    resources :doctors
    resources :specialists
    resources :clinics
    resources :specialist_clinics
    resources :doctor_clinics

    root to: "comments#index"
  end
end
