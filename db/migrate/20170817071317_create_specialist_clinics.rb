class CreateSpecialistClinics < ActiveRecord::Migration[5.1]
  def change
    create_table :specialist_clinics do |t|
      t.references :specialist, foreign_key: true
      t.references :clinic, foreign_key: true

      t.timestamps
    end
  end
end
