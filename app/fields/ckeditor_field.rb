require "administrate/field/base"

class CkeditorField < Administrate::Field::Base
  def to_s
    data
  end

  def html_safe
    data.html_safe
  end
end
