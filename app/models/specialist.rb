class Specialist < ApplicationRecord
  belongs_to :doctor
  has_many :specialist_clinics, dependent: :destroy
  has_many :clinics, through: :specialist_clinics
end
