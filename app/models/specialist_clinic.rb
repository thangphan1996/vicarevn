class SpecialistClinic < ApplicationRecord
  belongs_to :specialist
  belongs_to :clinic
end
