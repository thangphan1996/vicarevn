class CreateDoctors < ActiveRecord::Migration[5.1]
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :avatar
      t.string :job
      t.string :experience
      t.string :specialized
      t.string :address

      t.timestamps
    end
  end
end
