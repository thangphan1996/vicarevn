require "administrate/base_dashboard"

class ClinicDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    doctors: Field::HasMany,
    specialist_clinics: Field::HasMany,
    doctor_clinics: Field::HasMany,
    specialists: Field::HasMany,
    comments: Field::HasMany,
    id: Field::Number,
    name: Field::String,
    image: AvatarField,
    address: Field::String,
    service: Field::String,
    introduce: CkeditorField,
    time_open: Field::String,
    time_close: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :doctors,
    :name,
    :image,
    :service,
    :address,
    :time_open,
    :time_close,
    :comments,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :doctors,
    :specialist_clinics,
    :specialists,
    :comments,
    :id,
    :name,
    :image,
    :address,
    :service,
    :introduce,
    :time_open,
    :time_close,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :doctors,
    :specialist_clinics,
    :specialists,
    :comments,
    :image,
    :name,
    :address,
    :service,
    :introduce,
    :time_open,
    :time_close,
  ].freeze

  # Overwrite this method to customize how clinics are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(clinic)
    clinic.name.truncate(30)
  end
end
